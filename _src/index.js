import _ from "lodash";
import './style.css';
import Icon from "./icon.jpg";
import data from "./data.xml";
import mailing from "./mailing_list.csv";
import users from "./users.json";
import printMe from "./print";


function component () {
    var element = document.createElement("div");
    element.innerHTML = _.join(["Hellooo", "Webpack"], " ");
    element.classList.add('hello');

    var btn = document.createElement("button");

    var icon = new Image();
    icon.src = Icon;
    element.appendChild(icon);  

    btn.innerHTML = "Click me";
    btn.onclick = printMe;
    element.appendChild(btn);
    
    console.log(data);
    console.log(mailing);
    console.log(users);

    return element;
}
document.body.appendChild(component());

const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        app: "./_src/index.js",
        main: "./_src/index.ts",
        print: "./_src/print.js",

    },

    plugins:[
        new HtmlWebpackPlugin({
            title: 'webpack tools'
        })
    ],
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "_dist")
    },
    module: {
        rules: [
            {test: /\.tsx?$/, use: "ts-loader", exclude: /node_modules/},
            {test: /\.css$/,use: ["style-loader","css-loader"]},
            { test: /\.(png|svg|jpg|gif)$/, use: [{loader: "url-loader", options: {limit: 8000}}]},
            { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' },
            {test: /\.(csv|tsv)$/, use: ["csv-loader"]},
            {test: /\.xml$/, use: ["xml-loader"]},
        ]
    },

    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
};